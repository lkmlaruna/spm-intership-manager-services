var mongoose = require('mongoose');
const Schema = mongoose.Schema;

const foodSchema = Schema({
    name: String,
    Price: String
})

mongoose.model('food', foodSchema);
mongoose.connect('mongodb+srv://admin:1qaz2wsx3edc@spmdb-arqnf.mongodb.net/test?retryWrites=true', function (err) {
    if(err) {
        console.log(err);
        process.exit(-1);
    }
    console.log("database connection established");
})

module.exports = mongoose;
